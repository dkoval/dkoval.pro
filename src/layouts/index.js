import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import './index.css'

const Layout = ({ children, data }) => (
  <div>
    <Helmet
      title={data.site.siteMetadata.title}
      meta={[
        { name: 'description', content: 'Dima Koval - DevOps' },
        { name: 'keywords', content: 'DevOps, Programming, Go, Rails, Ruby' },
        { name: 'apple-mobile-web-app-title', content: 'Dima Koval - DevOps' },
        { name: 'application-name', content: 'Blog' },
        { name: 'msapplication-TileColor', content: '#ad2044' },
        { name: 'theme-color', content: '#ad2044' },
      ]}
    >
      <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro:400,700" rel="stylesheet"/>
      <link rel="apple-touch-icon" sizes="180x180" href="/static/apple-touch-icon.png" />
      <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon-32x32.png" />
      <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon-16x16.png" />
      <link rel="manifest" href="/static/site.webmanifest" />
      <link rel="mask-icon" href="/static/safari-pinned-tab.svg" color="#ad2044" />
    </Helmet>
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        paddingTop: '12em',
      }}
    >
      {children()}
    </div>
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
