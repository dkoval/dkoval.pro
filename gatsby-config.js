module.exports = {
  siteMetadata: {
    title: 'Dima Koval – Full-stack Web Developer & DevOps',
    description: 'Dima Koval – Full-stack Web Developer & DevOps'
  },
  plugins: ['gatsby-plugin-react-helmet'],
}
